# use clang-format
targets:
 * `clang-format` Shows which files are affected by clang-format
 * `check-clang-format` Errors if files are affected by clang-format (for CI integration)
 * `fix-clang-format` Applies clang-format to all affected files

# use clang-tidy
targets:
 * clang-tidy
 * fix-clang-tidy

# use run-clang-tidy.py (no colors, duplicate outputs, quicker)
targets:
 * clang-tidy-parallel
 * fix-clang-tidy-parallel
