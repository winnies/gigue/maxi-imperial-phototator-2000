# Maxi-Imperial Phototator 2000

Logiciel de retouche photo du futur.

## Sujet

Cette application permettra de choisir un ou plusieurs fichiers de photo ou d’image présents sur un disque dur à partir d’une arborescence. L’ensemble des photos sera proposé à l’utilisateur qui pourra choisir la ou les photos qu’il souhaite modifier. Si l’utilisateur travaille sur le fichier d’origine, une confirmation du lieu de l’enregistrement lui sera demandée. L’utilisateur pourra soit enregistrer les modifications directement dans le fichier lu, soit créer une copie du fichier modifié qu’il nommera. Toutes les modifications pourront être appliquées à une ou plusieurs photos. Les modifications principales sont le redimensionnement et le rognage des photos, l’application de transformation d’image ou de filtres sur une zone choisie ou sur la totalité de la photo. La sélection de zone se fera à l’aide de formes prédéfinies (triangle, quadrilatère, rectangle, carré, ellipse, étoile) et de formes libres. Il sera aussi possible de définir une zone par différents algorithmes comme le choix d’une couleur.

Quelques fonctionnalités additionnelles sont possibles. Un histogramme de distribution des couleurs sera proposé à l’utilisateur. Ce dernier pourra modifier la couleur associée à chaque barre de l’histogramme ce qui modifiera l’image. Lorsque la photo est agrandie au-delà de la taille de la fenêtre, une localisation de la zone visible apparaitra dans une vignette (carte mentale). L’ajout d’un texte doit être possible afin d’insérer des messages personnalisés.
