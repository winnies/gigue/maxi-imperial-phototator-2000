#include <QAction>
#include <QDebug>
#include <QFileDialog>
#include <QImageReader>
#include <QMenu>
#include <maxi-imperial-phototator-2000/workspace/workspace.hpp>

Workspace::Workspace(QWidget* parent) : QListWidget(parent) {
    // Array picture
    arrayPicture = QHash<QString, QListWidgetItem*>();

    setContextMenuPolicy(Qt::CustomContextMenu);
    connect(this, &QListWidget::customContextMenuRequested, this, &Workspace::showContextMenu);

    sendSelectionPicture = new QAction(tr("&Editer"), this);
    sendSelectionPicture->setStatusTip(tr("Editer les images"));
    connect(sendSelectionPicture, &QAction::triggered, this, &Workspace::sendSelectionItem);

    deleteAct = new QAction(tr("&Supprimer"), this);
    deleteAct->setStatusTip(tr("Supprime l'image"));
    connect(deleteAct, &QAction::triggered, this, &Workspace::deletePictureWorksapce);

    addPic = new QAction(tr("&Ajouter une image"), this);
    addPic->setStatusTip(tr("ajoute une image dans la zone de travail"));
    connect(addPic, &QAction::triggered, this, &Workspace::openDialogPicture);

    deleteAll = new QAction(tr("&Effacer tout"), this);
    deleteAll->setStatusTip(tr("Supprime toutes les images"));
    connect(deleteAll, &QAction::triggered, this, &Workspace::deleteAllPictureWorksapce);

    connect(this, &Workspace::itemSelectionChanged, this, &Workspace::hasSelect);
}

void Workspace::addPictureWorksapce(const QString& path) {
    auto* test = new QImageReader(path);
    if (!test->canRead()) {
        qDebug() << "Le chargement de l'image ne marche pas";
        if (arrayPicture.isEmpty()) {
            emit emptyList();
        }
        return;
    }

    QIcon icon       = QIcon(path);
    QString nameFile = path.section('/', -1);

    QFontMetrics metrics(font());
    QString elidedName = metrics.elidedText(nameFile, Qt::ElideLeft, getSizeIcons().rwidth() / getRatioSize());

    if (arrayPicture.contains(path)) {
        qDebug() << "L'image est déjà dans le workspace";
        return;
    }

    auto item = new QListWidgetItem(icon, elidedName);

    arrayPicture.insert(path, item);

    addItem(item);
    emit nbPictureListWidget(count());
    emit addPicture(path);
}

void Workspace::deletePictureWorksapce() {
    QList<QListWidgetItem*> listItem = selectedItems();
    for (auto item : listItem) {
        QString path = arrayPicture.key(item);
        if (arrayPicture.contains(path)) {
            qDebug() << "Supprime l'image : " << path;
            arrayPicture.remove(path);
            removeItemWidget(item);
            delete item;
            item = nullptr;
            emit removePicture();
            emit nbPictureListWidget(count());
            if (arrayPicture.empty()) {
                emit emptyList();
            }
        } else {
            qDebug() << "Erreur : l'image n'existe plus dans le workspace";
        }
    }
}

void Workspace::deleteAllPictureWorksapce() {
    qDebug() << "Supprime toutes les images";
    arrayPicture.clear();
    clear();
    emit emptyList();
    emit nbPictureListWidget(count());
}

void Workspace::showContextMenu(const QPoint& pos) {
    QPoint globalPos      = mapToGlobal(pos);
    QListWidgetItem* item = itemAt(pos);

    QMenu menu(this);

    if (item != nullptr) {
        menu.addAction(sendSelectionPicture);
        menu.addAction(deleteAct);
    } else {
        if (!selectedItems().isEmpty()) {
            menu.addAction(sendSelectionPicture);
        }
        menu.addAction(addPic);
        menu.addAction(deleteAll);
    }

    menu.exec(globalPos);
    emit nbPictureListWidget(count());
}

void Workspace::sendSelectionItem() {
    qDebug() << "Les images à editer";
    QList<QListWidgetItem*> items = selectedItems();
    QVector<QString> itemsPath    = QVector<QString>();
    for (auto item : items) {
        QString path = arrayPicture.key(item);
        if (arrayPicture.contains(path)) {
            itemsPath.append(path);
        }
    }
    emit sendListItems(itemsPath);
}

void Workspace::openDialogPicture() {
    QFileDialog dialog(this, tr("Open File"));
    dialog.setNameFilter(tr("Images (*.png *.xpm *.jpg *jpeg)"));
    dialog.setFileMode(QFileDialog::ExistingFiles);
    dialog.setOption(QFileDialog::ReadOnly);

    QStringList fileNames;

    if (dialog.exec() != 0) {
        fileNames = dialog.selectedFiles();
    } else {
        return;
    }

    QString path;
    for (auto p : fileNames) {
        path = p.toLocal8Bit().constData();
        addPictureWorksapce(path);
    }
}

void Workspace::hasSelect() {
    QList<QListWidgetItem*> listItem = selectedItems();
    if (listItem.isEmpty()) {
        emit noSelect();
    } else {
        emit select();
    }
}
