#include <QApplication>
#include <QContextMenuEvent>
#include <QDebug>
#include <QDropEvent>
#include <QFileDialog>
#include <QLabel>
#include <QMenuBar>
#include <QMimeData>
#include <QStatusBar>
#include <QVBoxLayout>
#include <maxi-imperial-phototator-2000/workspace/workspace.hpp>
#include <maxi-imperial-phototator-2000/workspace/workspacewidget.hpp>

WorkspaceWidget::WorkspaceWidget(QWidget* parent) : QWidget(parent) {
    //    Layout base
    baseLayout = new QVBoxLayout(this);
    baseLayout->setAlignment(Qt::AlignCenter);
    baseLayout->setContentsMargins(0, 0, 0, 0);

    //    CreateWorspace
    workspaceListPicture = new Workspace(this);
    workspaceListPicture->setFlow(QListView::LeftToRight);
    workspaceListPicture->setResizeMode(QListView::Adjust);
    workspaceListPicture->setSpacing(10);
    workspaceListPicture->setViewMode(QListView::IconMode);
    workspaceListPicture->setMovement(QListView::Static); // temporaire
    workspaceListPicture->setSizeIcons(size() / workspaceListPicture->getRatioSize());
    workspaceListPicture->setIconSize(size() / workspaceListPicture->getRatioSize());
    workspaceListPicture->setSelectionMode(QAbstractItemView::ExtendedSelection);
    workspaceListPicture->setUniformItemSizes(true);

    baseLayout->addWidget(workspaceListPicture);

    workspaceListPicture->hide();

    //    Label no picture
    noPicture = new QLabel("Ajoutez une photo dans l'espace de travail");
    noPicture->setAlignment(Qt::AlignCenter);

    baseLayout->addWidget(noPicture);

    setLayout(baseLayout);
    setAcceptDrops(true);

    // Workspace
    connect(workspaceListPicture, &Workspace::emptyList, this, &WorkspaceWidget::emptyWorkspace);

    connect(workspaceListPicture, &Workspace::nbPictureListWidget, this, &WorkspaceWidget::nbPictureListWidget);

    connect(workspaceListPicture, &Workspace::addPicture, this, &WorkspaceWidget::addPicture);
    connect(workspaceListPicture, &Workspace::removePicture, this, &WorkspaceWidget::removePicture);
    connect(workspaceListPicture, &Workspace::sendListItems, this, &WorkspaceWidget::sendListItems);
}

QMenuBar* WorkspaceWidget::getMenuBar() const {
    auto menu = new QMenuBar();

    auto fileMenu = new QMenu("&Fichier");
    fileMenu->menuAction()->setStatusTip("Fichier");

    auto openAct = fileMenu->addAction(QObject::tr("&Ouvrir..."), this, &WorkspaceWidget::openDialogPicture);
    openAct->setStatusTip("Ouvrir des images");

    fileMenu->addSeparator();

    auto exitAct = fileMenu->addAction(QObject::tr("&Quitter"), qApp, &QApplication::quit);
    exitAct->setStatusTip("Quitter");
    exitAct->setShortcut(QObject::tr("Ctrl+Q"));

    auto editAct = new QAction(QObject::tr("&Editer"));
    editAct->setStatusTip("Editer les fichiers");
    editAct->setDisabled(true);
    connect(workspaceListPicture, &Workspace::select, editAct, [editAct]() { editAct->setDisabled(false); });

    connect(workspaceListPicture, &Workspace::noSelect, editAct, [editAct]() { editAct->setDisabled(true); });
    connect(editAct, &QAction::triggered, workspaceListPicture, &Workspace::sendSelectionItem);

    menu->addMenu(fileMenu);
    menu->addAction(editAct);
    return menu;
}

QStatusBar* WorkspaceWidget::getStatusBar() {
    auto status = new QStatusBar();
    status->setSizeGripEnabled(true);

    status->showMessage("Pas d'images");
    status->setSizeGripEnabled(false);
    status->setAutoFillBackground(true);
    status->setBackgroundRole(QPalette::Dark);

    connect(workspaceListPicture, &Workspace::nbPictureListWidget, status, [status](int nb) {
        QString msg;
        if (nb == 0) {
            msg = "Pas d'images";
        } else {
            msg = QString::number(nb) + " images";
        }
        status->showMessage(msg);
    });

    connect(status, &QStatusBar::messageChanged, status, [status, this]() {
        if (status->currentMessage().isEmpty()) {
            auto nb = workspaceListPicture->getArrayPicture().size();
            QString msg;
            if (nb == 0) {
                msg = "Pas d'images";
            } else {
                msg = QString::number(nb) + " images";
            }
            status->showMessage(msg);
        }
    });

    auto sliderSize = new QSlider(Qt::Horizontal);
    sliderSize->setFocusPolicy(Qt::StrongFocus);
    sliderSize->setTickPosition(QSlider::TicksBothSides);
    sliderSize->setRange(2, 9);
    sliderSize->setValue(5);
    workspaceListPicture->setRatioSize(4);
    workspaceListPicture->setIconSize(size() / workspaceListPicture->getRatioSize());

    status->addPermanentWidget(sliderSize);
    connect(sliderSize, &QSlider::valueChanged, sliderSize, [sliderSize, this](int value) {
        workspaceListPicture->setRatioSize(sliderSize->maximum() + 2 - value);
        resizeItemEvent();
    });

    return status;
}

// SLOTS

void WorkspaceWidget::addPictureInWorskpace(const QString& path) {
    if (workspaceListPicture->isHidden()) {
        noPicture->hide();
        workspaceListPicture->show();
        setContextMenuPolicy(Qt::NoContextMenu);
    }
    workspaceListPicture->addPictureWorksapce(path);
    resizeItemEvent();
}

void WorkspaceWidget::emptyWorkspace() {
    setContextMenuPolicy(Qt::DefaultContextMenu);
    workspaceListPicture->hide();
    noPicture->show();
    emit workspaceEmpty();
}

void WorkspaceWidget::contextMenuEvent(QContextMenuEvent* event) {
    QMenu menu(this);
    menu.addAction(QObject::tr("&Ajouter une image"), this, &WorkspaceWidget::openDialogPicture);
    menu.exec(event->globalPos());
}

void WorkspaceWidget::openDialogPicture() {
    QFileDialog dialog(this, tr("Open File"));
    dialog.setNameFilter(tr("Images (*.png *.xpm *.jpg *jpeg)"));
    dialog.setFileMode(QFileDialog::ExistingFiles);
    dialog.setOption(QFileDialog::ReadOnly);

    QStringList fileNames;

    if (dialog.exec() != 0) {
        fileNames = dialog.selectedFiles();
    } else {
        return;
    }

    QString path;
    for (auto p : fileNames) {
        path = p.toLocal8Bit().constData();
        addPictureInWorskpace(path);
    }
}

void WorkspaceWidget::dropEvent(QDropEvent* event) {
    const QMimeData* file = event->mimeData();
    if (file->hasUrls()) {
        if (file->urls().isEmpty()) {
            return;
        }
        for (auto i : file->urls()) {
            QString fileName = i.toLocalFile();
            qDebug() << i.toLocalFile().toLocal8Bit().constData();
            addPictureInWorskpace(fileName);
            event->acceptProposedAction();
        }
    }
}

void WorkspaceWidget::dragEnterEvent(QDragEnterEvent* event) {
    if (event->mimeData()->hasUrls()) {
        event->acceptProposedAction();
    }
}

void WorkspaceWidget::resizeEvent(QResizeEvent* event) {
    resizeItemEvent();
}

void WorkspaceWidget::resizeItemEvent() {
    workspaceListPicture->setSizeIcons(size());
    workspaceListPicture->setIconSize(workspaceListPicture->getSizeIcons() / workspaceListPicture->getRatioSize());
    for (int i = 0; i < workspaceListPicture->count(); i++) {
        auto* item       = workspaceListPicture->item(i);
        QString path     = workspaceListPicture->getArrayPicture().key(item);
        QString nameFile = path.section('/', -1);
        QFontMetrics metrics(workspaceListPicture->font());
        QString elidedName = metrics.elidedText(nameFile, Qt::ElideLeft, workspaceListPicture->iconSize().rwidth());
        item->setText(elidedName);
    }
}

void WorkspaceWidget::reloadImage(const QVector<QString>& pathImages) {
    for (auto path : pathImages) {
        workspaceListPicture->arrayPicture.value(path)->setIcon(QIcon(path));
    }
}
