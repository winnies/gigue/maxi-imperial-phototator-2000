#include "ui_optionswidget.h"
#include <maxi-imperial-phototator-2000/transformations/optionswidget.hpp>

OptionsWidget::OptionsWidget(const QString& title, QWidget* parent) : QWidget(parent), ui(new Ui::OptionsWidget) {
    ui->setupUi(this);
    ui->title->setText(title);
}

OptionsWidget::~OptionsWidget() {
    delete ui;
}

void OptionsWidget::reset() {}

void OptionsWidget::canceled() {}

void OptionsWidget::applied() {}
