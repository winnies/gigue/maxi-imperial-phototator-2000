#include "ui_optionswidget.h"
#include "ui_scalewidget.h"
#include <QMenuBar>
#include <maxi-imperial-phototator-2000/transformations/scale.hpp>

ScaleWidget::Options::Options(const QSize& size,
                              const Qt::AspectRatioMode aspectRatioMode,
                              const Qt::TransformationMode transformMode)
: size(size), aspectRatioMode(aspectRatioMode), transformMode(transformMode) {}

ScalePlugin::ScalePlugin(Editor* editor) : EditorPlugin(editor) {
    QAction* action = editor->menuBar()->addAction("Redimensionner");
    connect(action, &QAction::triggered, this, &ScalePlugin::triggered);
}

ScalePlugin::~ScalePlugin() {
    delete scaleWidget.data();
}

void ScalePlugin::triggered() {
    editor->reset();

    delete scaleWidget.data();
    scaleWidget = new ScaleWidget(editor);

    editor->setOptionsWidget(scaleWidget);
}

ScaleWidget::ScaleWidget(Editor* editor, QWidget* parent)
: OptionsWidget(name(), parent), ui(new Ui::ScaleWidget), editor(editor), options(editor->currentImage().size()) {
    ui->setupUi(OptionsWidget::ui->scrollAreaWidgetContents);
    optionsToWidget();

    connect(ui->width, qOverload<int>(&QSpinBox::valueChanged), this, &ScaleWidget::optionsChanged);
    connect(ui->height, qOverload<int>(&QSpinBox::valueChanged), this, &ScaleWidget::optionsChanged);
    connect(ui->aspectRatio, qOverload<int>(&QComboBox::currentIndexChanged), this, &ScaleWidget::optionsChanged);
    connect(ui->transform, qOverload<int>(&QComboBox::currentIndexChanged), this, &ScaleWidget::optionsChanged);
}

ScaleWidget::~ScaleWidget() {
    delete ui;
}

QString ScaleWidget::name() const {
    return tr("Redimensionner");
}

QImage ScaleWidget::transform(const QImage& source) const {
    return source.scaled(options.size, options.aspectRatioMode, options.transformMode);
}

void ScaleWidget::optionsToWidget() {
    ui->width->setValue(options.size.width());
    ui->height->setValue(options.size.height());

    int aspectRationIndex = -1;
    switch (options.aspectRatioMode) {
    case Qt::IgnoreAspectRatio:
        aspectRationIndex = 0;
        break;
    case Qt::KeepAspectRatio:
        aspectRationIndex = 1;
        break;
    case Qt::KeepAspectRatioByExpanding:
        aspectRationIndex = 2;
        break;
    }
    ui->aspectRatio->setCurrentIndex(aspectRationIndex);

    int transformIndex = -1;
    switch (options.transformMode) {
    case Qt::FastTransformation:
        transformIndex = 0;
        break;
    case Qt::SmoothTransformation:
        transformIndex = 1;
        break;
    }
    ui->transform->setCurrentIndex(transformIndex);
}

void ScaleWidget::widgetToOptions() {
    options.size.setWidth(ui->width->value());
    options.size.setHeight(ui->height->value());

    Qt::AspectRatioMode aspectRatioMode = Qt::IgnoreAspectRatio;
    switch (ui->aspectRatio->currentIndex()) {
    case 0:
        aspectRatioMode = Qt::IgnoreAspectRatio;
        break;
    case 1:
        aspectRatioMode = Qt::KeepAspectRatio;
        break;
    case 2:
        aspectRatioMode = Qt::KeepAspectRatioByExpanding;
        break;
    default:
        qWarning("Unknown aspectRatio combobox index");
        break;
    }
    options.aspectRatioMode = aspectRatioMode;

    Qt::TransformationMode tranformationMode = Qt::FastTransformation;
    switch (ui->transform->currentIndex()) {
    case 0:
        tranformationMode = Qt::FastTransformation;
        break;
    case 1:
        tranformationMode = Qt::SmoothTransformation;
        break;
    default:
        qWarning("Unknown tranformationMode combobox index");
        break;
    }
    options.transformMode = tranformationMode;
}

void ScaleWidget::reset() {
    options = Options(editor->currentImage().size());
    optionsToWidget();
}

void ScaleWidget::canceled() {
    editor->reset();
    deleteLater();
}

void ScaleWidget::applied() {
    editor->pushTransformation(transform(editor->currentImage()), name());
    editor->reset();
    deleteLater();
}

void ScaleWidget::optionsChanged() {
    widgetToOptions();
    editor->setImageShown(transform(editor->currentImage()));
}
