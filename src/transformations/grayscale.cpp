#include <QMenuBar>
#include <maxi-imperial-phototator-2000/edition/selectrect.hpp>
#include <maxi-imperial-phototator-2000/transformations/grayscale.hpp>

GrayscalePlugin::GrayscalePlugin(Editor* editor) : EditorPlugin(editor) {
    QAction* act = editor->menuBar()->addAction("Niveaux de gris");
    connect(act, &QAction::triggered, this, &GrayscalePlugin::triggered);
}

void GrayscalePlugin::triggered() {
    editor->reset();
    const QImage& source = editor->currentImage();

    QImage result(source);
    for (int y = 0; y < source.height(); ++y) {
        for (int x = 0; x < source.width(); ++x) {
            QRgb rgb = source.pixel(x, y);
            int gray = qGray(rgb);
            if (editor->selectionContains({x / (double)source.width(), y / (double)source.height()})) {
                result.setPixel(x, y, qRgb(gray, gray, gray));
            }
        }
    }

    editor->pushTransformation(result, "Niveaux de gris");
}
