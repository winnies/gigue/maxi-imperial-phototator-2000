#include <QApplication>
#include <maxi-imperial-phototator-2000/application.hpp>

int main(int argc, char* argv[]) {
    QApplication qapp(argc, argv);

    Application app;
    app.resize(500, 500); // NOLINT
    app.show();

    return qapp.exec(); // NOLINT
}
