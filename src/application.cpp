#include <QCloseEvent>
#include <QStackedWidget>
#include <maxi-imperial-phototator-2000/application.hpp>
#include <maxi-imperial-phototator-2000/edition/edition.hpp>
#include <maxi-imperial-phototator-2000/workspace/workspacewidget.hpp>

Application::Application(QWidget* parent) : QMainWindow(parent) {
    setWindowTitle("Maxi-Imperial Phototator 2000");

    workspaceWidget = new WorkspaceWidget(this);
    workspaceWidget->resize(100, 100);
    workspaceWidget->setMinimumSize(100, 100);

    stackWidget = new QStackedWidget(this);
    stackWidget->addWidget(workspaceWidget);
    toWorkspaceWidget();
    setCentralWidget(stackWidget);

    connect(workspaceWidget, &WorkspaceWidget::sendListItems, this, &Application::toEditionWidget);
}

void Application::toEditionWidget(const QVector<QString>& imagesPath) {
    editionWidget = new Edition(imagesPath, this);
    editionWidget->setBackgroundRole(QPalette::Dark);
    editionWidget->setAutoFillBackground(true);
    stackWidget->addWidget(editionWidget);

    connect(editionWidget, &Edition::swapBack, this, &Application::toWorkspaceWidget);
    connect(editionWidget, &Edition::saveTransformation, workspaceWidget, &WorkspaceWidget::reloadImage);

    stackWidget->setCurrentWidget(editionWidget);
    setMenuBar(editionWidget->getMenuBar());
    setStatusBar(editionWidget->getStatusBar());
}

void Application::toWorkspaceWidget() {
    stackWidget->setCurrentWidget(workspaceWidget);
    setMenuBar(workspaceWidget->getMenuBar());
    setStatusBar(workspaceWidget->getStatusBar());
}

void Application::closeEvent(QCloseEvent* event) {
    if (stackWidget->currentIndex() == stackWidget->indexOf(editionWidget)) {
        if (!editionWidget->saveImages())
            event->ignore();
    }
}
