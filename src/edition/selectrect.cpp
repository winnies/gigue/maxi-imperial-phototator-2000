#include <cmath>
#include <maxi-imperial-phototator-2000/edition/selectrect.hpp>

SelectRect::SelectRect(QWidget* parent) : QWidget(parent), target(nullptr) {
    // setSelection(QRect(10.0, 20.0, 80.0, 60.0));
    // relativeSelection = mapFromGlobal(QRect(10.0, 20.0, 80.0, 60.0));
    relativeSelection = QRectF{0, 0, 1, 1};

    pen = QPen();
}

bool SelectRect::contains(const QPointF& p) {
    return relativeSelection.contains(p);
}

void SelectRect::changeColor(int i) {
    qDebug() << i;
    switch (i) {
    case 0:
        pen.setColor(Qt::black);
        break;
    case 1:
        pen.setColor(Qt::red);
        break;
    case 2:
        pen.setColor(Qt::blue);
        break;
    case 3:
        pen.setColor(Qt::green);
        break;
    case 4:
        pen.setColor(Qt::yellow);
        break;
    case 5:
        pen.setColor(Qt::white);
        break;
    default:
        pen.setColor(Qt::black);
        break;
    }
    repaint();
}

void SelectRect::setTarget(QWidget* target) {
    this->target = target;
}

QRect makePoignee(QPoint centre, int largeur) {
    QPoint topLeft(centre.x() - largeur / 2, centre.y() - largeur / 2);
    QSize size(largeur, largeur);
    return QRect{topLeft, size};
}

bool isDansPoignee(QPoint curseur, QPoint centre, int largeur) {
    return makePoignee(centre, largeur).contains(curseur);
}

// void SelectRect::setSelection(QRect selection) {
//    this->absoluteSelection = selection;
//    middleTop       = QPoint(selection.left() + selection.width() / 2, selection.topLeft().y());
//    middleBottom    = QPoint(selection.left() + selection.width() / 2, selection.bottomLeft().y());
//    middleLeft      = QPoint(selection.left(), selection.bottomLeft().y() - selection.height() / 2);
//    middleRight     = QPoint(selection.right(), selection.bottomRight().y() - selection.height() / 2);
//}

void SelectRect::paintEvent(QPaintEvent* event) {
    QPainter painter(this);

    painter.translate(-QWidget::mapToGlobal(pos()));

    QRect absoluteSelection = mapToGlobal(relativeSelection);
    auto selection          = absoluteSelection;
    auto middleTop          = QPoint(selection.left() + selection.width() / 2, selection.topLeft().y());
    auto middleBottom       = QPoint(selection.left() + selection.width() / 2, selection.bottomLeft().y());
    auto middleLeft         = QPoint(selection.left(), selection.bottomLeft().y() - selection.height() / 2);
    auto middleRight        = QPoint(selection.right(), selection.bottomRight().y() - selection.height() / 2);

    pen.setStyle(Qt::DashLine);
    painter.setPen(pen);
    painter.drawRect(absoluteSelection);
    pen.setStyle(Qt::SolidLine);
    painter.setPen(pen);
    painter.drawRect(makePoignee(absoluteSelection.topLeft(), taillepoignee));
    painter.drawRect(makePoignee(absoluteSelection.topRight(), taillepoignee));
    painter.drawRect(makePoignee(absoluteSelection.bottomLeft(), taillepoignee));
    painter.drawRect(makePoignee(absoluteSelection.bottomRight(), taillepoignee));
    painter.drawRect(makePoignee(absoluteSelection.center(), taillepoignee));
    painter.drawRect(makePoignee(middleRight, taillepoignee));
    painter.drawRect(makePoignee(middleLeft, taillepoignee));
    painter.drawRect(makePoignee(middleTop, taillepoignee));
    painter.drawRect(makePoignee(middleBottom, taillepoignee));
}

void SelectRect::mousePressEvent(QMouseEvent* event) {
    QRect absoluteSelection = mapToGlobal(relativeSelection);
    auto selection          = absoluteSelection;
    auto middleTop          = QPoint(selection.left() + selection.width() / 2, selection.topLeft().y());
    auto middleBottom       = QPoint(selection.left() + selection.width() / 2, selection.bottomLeft().y());
    auto middleLeft         = QPoint(selection.left(), selection.bottomLeft().y() - selection.height() / 2);
    auto middleRight        = QPoint(selection.right(), selection.bottomRight().y() - selection.height() / 2);

    if (isDansPoignee(event->globalPos(), absoluteSelection.bottomRight(), taillepoignee)) {
        selectState = SelectState::BottomRight;
    } else if (isDansPoignee(event->globalPos(), absoluteSelection.bottomLeft(), taillepoignee)) {
        selectState = SelectState::BottomLeft;
    } else if (isDansPoignee(event->globalPos(), absoluteSelection.topLeft(), taillepoignee)) {
        selectState = SelectState::TopLeft;
    } else if (isDansPoignee(event->globalPos(), absoluteSelection.topRight(), taillepoignee)) {
        selectState = SelectState::TopRight;
    } else if (isDansPoignee(event->globalPos(), absoluteSelection.center(), taillepoignee)) {
        selectState = SelectState::Center;
    } else if (isDansPoignee(event->globalPos(), middleTop, taillepoignee)) {
        selectState = SelectState::middleTop;
    } else if (isDansPoignee(event->globalPos(), middleBottom, taillepoignee)) {
        selectState = SelectState::middleBottom;
    } else if (isDansPoignee(event->globalPos(), middleLeft, taillepoignee)) {
        selectState = SelectState::middleLeft;
    } else if (isDansPoignee(event->globalPos(), middleRight, taillepoignee)) {
        selectState = SelectState::middleRight;
    }
}

void SelectRect::mouseReleaseEvent(QMouseEvent* event) {
    selectState = SelectState::None;
}

void SelectRect::mouseMoveEvent(QMouseEvent* event) {
    //    auto c = QWidget::mapToGlobal(event->pos());
    //    auto p = mapFromGlobal(c);
    //    auto pp = mapToGlobal(p);
    //    auto ppp = mapFromGlobal(pp);
    //    qDebug() << c << p << pp << ppp;

    QRect absoluteSelection = mapToGlobal(relativeSelection);
    auto selection          = absoluteSelection;
    auto middleTop          = QPoint(selection.left() + selection.width() / 2, selection.topLeft().y());
    auto middleBottom       = QPoint(selection.left() + selection.width() / 2, selection.bottomLeft().y());
    auto middleLeft         = QPoint(selection.left(), selection.bottomLeft().y() - selection.height() / 2);
    auto middleRight        = QPoint(selection.right(), selection.bottomRight().y() - selection.height() / 2);

    switch (selectState) {
    case SelectState::BottomRight:
        absoluteSelection.setBottomRight(event->globalPos());
        break;
    case SelectState::BottomLeft:
        absoluteSelection.setBottomLeft(event->globalPos());
        break;
    case SelectState::TopLeft:
        absoluteSelection.setTopLeft(event->globalPos());
        break;
    case SelectState::TopRight:
        absoluteSelection.setTopRight(event->globalPos());
        break;
    case SelectState::Center:
        absoluteSelection.moveCenter(event->globalPos());
        break;
    case SelectState::middleTop:
        absoluteSelection.setTop(event->globalPos().y());
        break;
    case SelectState::middleBottom:
        absoluteSelection.setBottom(event->globalPos().y());
        break;
    case SelectState::middleLeft:
        absoluteSelection.setLeft(event->globalPos().x());
        break;
    case SelectState::middleRight:
        absoluteSelection.setRight(event->globalPos().x());
        break;
    case SelectState::None:
        break;
    }

    // setSelection(absoluteSelection);
    relativeSelection = mapFromGlobal(absoluteSelection);
    repaint();
}

QPoint SelectRect::mapToGlobal(const QPointF& p) const {
    QRect global(QWidget::mapToGlobal(target->pos()), target->size());

    auto v = QVector2D(p);
    auto r = v * QVector2D(global.bottomRight()) + (QVector2D{1.f, 1.f} - v) * QVector2D(global.topLeft());
    return {(int)std::ceil(r.x()), (int)std::ceil(r.y())};
}

QPointF SelectRect::mapFromGlobal(const QPoint& p) const {
    return ((QVector2D(p) - QVector2D(QWidget::mapToGlobal(target->pos()))) / QVector2D(target->width(), target->height()))
        .toPointF();
}

QRect SelectRect::mapToGlobal(const QRectF& p) const {
    return {mapToGlobal(p.topLeft()), mapToGlobal(p.bottomRight())};
}

QRectF SelectRect::mapFromGlobal(const QRect& p) const {
    return {mapFromGlobal(p.topLeft()), mapFromGlobal(p.bottomRight())};
}
