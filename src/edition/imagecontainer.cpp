#include <maxi-imperial-phototator-2000/edition/imagecontainer.hpp>

#include <QDebug>
#include <QImageReader>

ImageContainer::ImageContainer(QWidget* parent)
: QWidget(parent)
, mainBox(new QVBoxLayout(this))
, container(new QListWidget(this))
, imageSelect(nullptr)
, arrayPicture(QHash<QString, QListWidgetItem*>()) {
    container->setFlow(QListView::LeftToRight);
    container->setResizeMode(QListView::Fixed);
    container->setSpacing(4);
    container->setLayoutMode(QListView::Batched);
    container->setViewMode(QListView::IconMode);
    container->setMovement(QListView::Static);
    container->setSelectionMode(QAbstractItemView::SingleSelection);
    container->setWrapping(false);
    container->setUniformItemSizes(false);
    container->setIconSize(QSize(100, 100));
    container->setGridSize(QSize(container->gridSize().rwidth(), 400));
    resize(width(), 100);

    container->setStyleSheet(""
                             "QListView {show-decoration-selected: 1;}"
                             "");

    connect(container, &QListWidget::itemClicked, this, &ImageContainer::selectImage);

    setLayout(mainBox);
    mainBox->addWidget(container);
}

void ImageContainer::addPictureWorksapce(QString& path) {
    auto* test = new QImageReader(path);
    if (!test->canRead()) {
        qDebug() << "Le chargement de l'image ne marche pas";
        return;
    }

    QIcon icon       = QIcon(path);
    QString nameFile = path.section('/', -1);

    QFontMetrics metrics(font());
    QString elidedName = metrics.elidedText(nameFile, Qt::ElideRight, container->iconSize().rwidth());

    auto item = new QListWidgetItem(icon, elidedName);
    arrayPicture.insert(path, item);
    container->addItem(item);
    container->setCurrentItem(item);
    imageSelect = item;
}

void ImageContainer::selectImage(QListWidgetItem* item) {
    imageSelect->setBackground(Qt::NoBrush);
    imageSelect->setSelected(false);
    imageSelect = nullptr;
    imageSelect = item;
    auto brush  = QBrush(QColor(223, 233, 68));
    imageSelect->setSelected(true);
    imageSelect->setBackground(brush);
    emit imageSelected(arrayPicture.key(item));
}

void ImageContainer::resizeEvent(QResizeEvent* event) {
    if (height() <= 370) {
        container->setIconSize(QSize(height() - 20, height() - 20));
        QHashIterator<QString, QListWidgetItem*> item(arrayPicture);
        while (item.hasNext()) {
            item.next();
            auto path     = item.key();
            auto nameFile = path.section('/', -1);
            QFontMetrics metrics(font());
            auto elidedName = metrics.elidedText(nameFile, Qt::ElideRight, container->iconSize().rwidth());
            item.value()->setText(elidedName);
        }
    }
}
