#include <QMenuBar>
#include <QMessageBox>
#include <QPushButton>
#include <QSplitter>
#include <QStackedWidget>
#include <QToolBar>
#include <QVector>

#include <maxi-imperial-phototator-2000/edition/buttoncontainer.hpp>
#include <maxi-imperial-phototator-2000/edition/edition.hpp>
#include <maxi-imperial-phototator-2000/edition/imagecontainer.hpp>
#include <maxi-imperial-phototator-2000/edition/widgetimage.hpp>
#include <maxi-imperial-phototator-2000/transformations/grayscale.hpp>
#include <maxi-imperial-phototator-2000/transformations/scale.hpp>

Edition::Edition(QWidget* parent)
: Editor(parent)
, m_menuBar(new QMenuBar(this))
, m_layout(new QVBoxLayout(this))
, m_toolBar(new QToolBar(this))
, m_widgetImage(new WidgetImage(this))
, m_stackedWidget(new QStackedWidget(this))
, imgContainer(new ImageContainer(this)) {
    auto* splitter = new QSplitter(this);
    splitter->setOrientation(Qt::Vertical);

    auto* splitter2 = new QSplitter(this);

    splitter2->addWidget(m_widgetImage);
    splitter2->addWidget(m_stackedWidget);
    splitter2->setCollapsible(0, false);

    splitter->addWidget(splitter2);
    splitter->addWidget(imgContainer);
    splitter->setCollapsible(0, false);

    layout()->addWidget(splitter);

    QList<int> sizes;
    sizes.append(0.7 * width());
    sizes.append(0.4 * width());
    splitter2->setSizes(sizes);

    ButtonContainer* buttons = new ButtonContainer();

    connect(imgContainer, &ImageContainer::imageSelected, this, &Edition::setCurrentPathImage);
    splitter->setStretchFactor(0, 1);
    splitter->setStretchFactor(1, 0);

    sizes.clear();
    sizes.append(100);
    sizes.append(imgContainer->height() + 25);
    splitter->setSizes(sizes);

    m_optionsWidget = m_stackedWidget->layout();
    m_optionsWidget->addWidget(buttons);

    new ScalePlugin(this);
    new GrayscalePlugin(this);

    connect(buttons, &ButtonContainer::selectionPressed, m_widgetImage, &WidgetImage::setSelection);
    connect(buttons, SIGNAL(colorChanged(int)), m_widgetImage, SLOT(changeSelectionColor(int)));

    QAction* ret = m_menuBar->addAction("Retour espace de travail");

    connect(ret, &QAction::triggered, this, &Edition::returnWorskpace);

    imageChange = false;
}

Edition::Edition(const QVector<QString>& imagesPath, QWidget* parent) : Edition(parent) {
    auto path = QString();
    for (QString imageName : imagesPath) {
        m_images.insert(imageName, QImage(imageName));
        imgContainer->addPictureWorksapce(imageName);
        path = imageName;
    }

    imgContainer->selectImage(imgContainer->container->currentItem());
    m_currentPathImage = path;
}

Edition::~Edition() = default;

QMenuBar* Edition::menuBar() {
    return m_menuBar;
}

QToolBar* Edition::toolBar() {
    return m_toolBar;
}

QMenuBar* Edition::getMenuBar() {
    return m_menuBar;
}

QStatusBar* Edition::getStatusBar() {
    return nullptr;
}

const QImage& Edition::currentImage() const {
    return m_currentImage;
}

void Edition::setImageShown(const QImage& image) {
    m_widgetImage->setImage(image);
}

void Edition::setCurrentImage(const QImage& image) {
    m_currentImage               = image;
    m_images[m_currentPathImage] = image;
    setImageShown(m_currentImage);
}

void Edition::setCurrentPathImage(const QString& imagePath) {
    m_currentPathImage = imagePath;
    setCurrentImage(m_images[imagePath]);
}

void Edition::pushTransformation(const QImage& image, const QString& description) {
    imageChange = true;
    transformations.emplace_back(image, description);
    setCurrentImage(image);
}

void Edition::setOptionsWidget(QWidget* widget) {
    if (options != nullptr) {
        m_optionsWidget->removeWidget(options);
        options->setParent(nullptr);
    }
    m_optionsWidget->addWidget(options = widget);
    m_stackedWidget->setCurrentIndex(1);
}

void Edition::setToolOptionsWidget(QWidget* widget) {}

bool Edition::selectionContains(const QPointF& p) {
    return m_widgetImage->selectionContains(p);
}

void Edition::reset() {
    m_stackedWidget->setCurrentIndex(0);
    setImageShown(currentImage());
}

void Edition::returnWorskpace() {
    if (!saveImages())
        return;
    emit swapBack();
}

bool Edition::saveImages() {
    if (imageChange) {
        QMessageBox saveDialog(this);
        saveDialog.setText("Des images ont été modifiées.");
        saveDialog.setInformativeText("Voulez-vous sauvegarder les changements ?");
        saveDialog.setStandardButtons(QMessageBox::Discard | QMessageBox::Cancel | QMessageBox::Save);
        saveDialog.setDefaultButton(QMessageBox::Save);

        auto result = saveDialog.exec();

        if (result == QMessageBox::Save) {
            QHash<QString, QImage>::iterator i;
            for (i = m_images.begin(); i != m_images.end(); ++i) {
                i.value().save(i.key());
            }
            emit saveTransformation(m_images.keys().toVector());
        } else if (result == QMessageBox::Cancel) {
            return false;
        }
        imageChange = false;
    }
    return true;
}
