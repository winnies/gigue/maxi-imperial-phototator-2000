#include <maxi-imperial-phototator-2000/edition/imagescrollarea.hpp>
#include <maxi-imperial-phototator-2000/edition/widgetimage.hpp>

#include <QWheelEvent>

ImageScrollArea::ImageScrollArea(QWidget* parent) : QScrollArea(parent) {}

void ImageScrollArea::wheelEvent(QWheelEvent* event) {
    if (event->modifiers().testFlag(Qt::ControlModifier)) {
        // WidgetImage::wheelEvent(event);
    } else {
        QScrollArea::wheelEvent(event);
    }
}
