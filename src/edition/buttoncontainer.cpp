#include "ui_buttoncontainer.h"
#include <maxi-imperial-phototator-2000/edition/buttoncontainer.hpp>
#include <maxi-imperial-phototator-2000/edition/choosecolor.hpp>
#include <maxi-imperial-phototator-2000/edition/edition.hpp>

#include <QGridLayout>
#include <QPushButton>

ButtonContainer::ButtonContainer(QWidget* parent) : QWidget(parent), ui(new Ui::ButtonContainer()) {
    ui->setupUi(this);

    m_chooseColor = new ChooseColor(ui->page_2);

    m_optionsWidget = ui->stackedWidget->layout();

    connect(ui->selectionButton, &QPushButton::pressed, this, &ButtonContainer::selectionPressed);
    connect(ui->selectionButton, &QPushButton::pressed, this, &ButtonContainer::swapStack);
    connect(m_chooseColor, SIGNAL(colorChanged(int)), this, SIGNAL(colorChanged(int)));
}

ButtonContainer::~ButtonContainer() {
    delete ui;
}

void ButtonContainer::swapStack() {
    if (ui->stackedWidget->currentIndex() == 1) {
        ui->stackedWidget->setCurrentIndex(0);
    } else if (ui->stackedWidget->currentIndex() == 0) {
        ui->stackedWidget->setCurrentIndex(1);
    }
}
