#include <maxi-imperial-phototator-2000/edition/selectrect.hpp>
#include <maxi-imperial-phototator-2000/edition/widgetimage.hpp>

WidgetImage::WidgetImage(QWidget* parent)
: QWidget(parent)
, m_imageLabel(new QLabel)
, m_scrollArea(new ImageScrollArea)
, m_layout(new QVBoxLayout)
, m_selection(new SelectRect) {
    m_imageLabel->setBackgroundRole(QPalette::Dark);
    m_imageLabel->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
    auto font = m_imageLabel->font();
    font.setStyleStrategy(QFont::NoAntialias);
    m_imageLabel->setFont(font);
    m_imageLabel->setScaledContents(true);

    m_scrollArea->setBackgroundRole(QPalette::Dark);
    m_scrollArea->setWidget(m_imageLabel);

    m_layout->addWidget(m_scrollArea);
    setLayout(m_layout);

    m_selection->setParent(m_scrollArea->viewport());
    m_selection->setTarget(m_imageLabel);
    m_selection->hide();

    new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_Plus), this, SLOT(zoomIn()));
    new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_Minus), this, SLOT(zoomOut()));
}

bool WidgetImage::selectionContains(const QPointF& p) {
    if (m_selection->isHidden())
        return true;
    return m_selection->contains(p);
}

void WidgetImage::setImage(const QImage& image) {
    QPixmap pixmap(QPixmap::fromImage(image));
    m_imageLabel->setPixmap(pixmap);
    m_imageLabel->resize(pixmap.size());
    // m_selection->resize(m_imageLabel->size());
}

void WidgetImage::changeSelectionColor(int i) {
    m_selection->changeColor(i);
}

void WidgetImage::setSelection() {
    if (m_selection->isHidden()) {
        // m_selection = new SelectRect(m_imageLabel);
        m_selection->show();
        // m_selection->resize(m_imageLabel->size());
    } else {
        m_selection->hide();
    }
}

void WidgetImage::wheelEvent(QWheelEvent* event) {
    if (event->modifiers().testFlag(Qt::ControlModifier)) {
        if (event->delta() > 0) {
            zoomIn();
        } else if (event->delta() < 0) {
            zoomOut();
        }
    }
}

void WidgetImage::resizeEvent(QResizeEvent* event) {
    m_selection->resize(m_scrollArea->viewport()->size());
}

void WidgetImage::zoomIn() {
    scaleImage(1.25);
}

void WidgetImage::zoomOut() {
    scaleImage(0.8);
}

void WidgetImage::scaleImage(double factor) {
    Q_ASSERT(m_imageLabel->pixmap());
    scaleFactor *= factor;
    auto size = scaleFactor * m_imageLabel->pixmap()->size();
    m_imageLabel->resize(size);
    // m_selection->resize(m_imageLabel->size());
}
