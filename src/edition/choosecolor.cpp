#include "ui_choosecolor.h"
#include <maxi-imperial-phototator-2000/edition/choosecolor.hpp>

#include <QColor>
#include <QDebug>
#include <QPalette>

ChooseColor::ChooseColor(QWidget* parent) : QWidget(parent), ui(new Ui::ChooseColor()) {
    ui->setupUi(this);

    connect(ui->comboBox, SIGNAL(currentIndexChanged(int)), this, SIGNAL(colorChanged(int)));
}

ChooseColor::~ChooseColor() {
    delete ui;
}
