# Dependencies

Qt 5.12 <
extra-cmake-modules v5.63.0 (https://api.kde.org/frameworks/extra-cmake-modules/html/index.html)

# Build and run

```
git clone https://gitlab.com/winnies/gigue/maxi-imperial-phototator-2000.git
cd maxi-imperial-phototator-2000
mkdir build && cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make -j$(nproc)
./maxi-imperial-phototator-2000
```

# Build at university

```
cd maxi-imperial-phototator-2000
source ./scripts/setup_at_university.sh
```

# Build docker image and publish

```
docker login registry.gitlab.com
docker build -t registry.gitlab.com/winnies/gigue/maxi-imperial-phototator-2000 .
docker push registry.gitlab.com/winnies/gigue/maxi-imperial-phototator-2000
```

# Run ci

```
gitlab-runner exec docker build
```

With local image:

```
gitlab-runner exec docker --docker-pull-policy=never build
```
