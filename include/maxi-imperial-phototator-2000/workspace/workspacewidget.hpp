#ifndef MAXI_IMPERIAL_PHOTOTATOR_2000_WORKSPACE_WORKSPACEWIDGET_HPP
#define MAXI_IMPERIAL_PHOTOTATOR_2000_WORKSPACE_WORKSPACEWIDGET_HPP

#include <QWidget>

class QMenuBar;
class QStatusBar;
class QVBoxLayout;
class QLabel;
class Workspace;

class WorkspaceWidget : public QWidget {
    Q_OBJECT
  public:
    WorkspaceWidget(QWidget* parent = nullptr);
    ~WorkspaceWidget() override = default;

    QMenuBar* getMenuBar() const;
    QStatusBar* getStatusBar();

  private:
    // Base widget
    QVBoxLayout* baseLayout;

    // No picture
    QLabel* noPicture;

    Workspace* workspaceListPicture;

  public slots:
    void openDialogPicture();
    void addPictureInWorskpace(const QString& path);
    void reloadImage(const QVector<QString>& pathImages);

  private slots:
    void resizeItemEvent();
    void resizeEvent(QResizeEvent* event) override;
    void emptyWorkspace();
    void contextMenuEvent(QContextMenuEvent* event) override;
    void dragEnterEvent(QDragEnterEvent* event) override;
    void dropEvent(QDropEvent* event) override;

  signals:

    // Worskpace
    void workspaceEmpty();
    void nbPictureListWidget(int nb);

    // Picture
    void addPicture(const QString& path);
    void removePicture();

    // List
    void sendListItems(const QVector<QString>& itemsPath);
};
#endif
