#ifndef MAXI_IMPERIAL_PHOTOTATOR_2000_WORKSPACE_WORKSPACE_HPP
#define MAXI_IMPERIAL_PHOTOTATOR_2000_WORKSPACE_WORKSPACE_HPP

#include <QListWidget>
#include <QWidget>

class Workspace : public QListWidget {
    Q_OBJECT
  public:
    Workspace(QWidget* parent = nullptr);

    void addPictureWorksapce(const QString& path);

    void setRatioSize(int ratio) { ratioSize = ratio; }
    int getRatioSize() const { return ratioSize; }

    void setSizeIcons(QSize s) { sizeIcons = s; }
    QSize getSizeIcons() const { return sizeIcons; }
    QHash<QString, QListWidgetItem*> arrayPicture;

    inline const QHash<QString, QListWidgetItem*>& getArrayPicture() const { return arrayPicture; }

  private:
    // Var

    // Menu
    QAction* sendSelectionPicture;
    QAction* deleteAct;

    // On item
    int ratioSize = 4;
    QSize sizeIcons;

    // On workspace
    QAction* addPic;
    QAction* deleteAll;

  public slots:
    void sendSelectionItem();

  private slots:
    void showContextMenu(const QPoint& pos);
    void openDialogPicture();
    void deletePictureWorksapce();
    void deleteAllPictureWorksapce();
    void hasSelect();

  signals:
    // Picture
    void addPicture(QString path);
    void nbPictureListWidget(int nb);
    void removePicture();

    // List
    void select();
    void noSelect();

    void sendListItems(const QVector<QString>& itemsPath);
    void emptyList();
};

#endif
