#ifndef MAXI_IMPERIAL_PHOTOTATOR_2000_EDITOR_HPP
#define MAXI_IMPERIAL_PHOTOTATOR_2000_EDITOR_HPP

#include <QImage>
#include <QWidget>

class QMenuBar;
class QToolBar;
class SelectRect;

class Editor : public QWidget {
    Q_OBJECT
  public:
    using QWidget::QWidget;

    virtual ~Editor()                                                                                     = default;
    virtual QMenuBar* menuBar()                                                                           = 0;
    virtual QToolBar* toolBar()                                                                           = 0;
    virtual const QImage& currentImage() const                                                            = 0;
    virtual void setImageShown(const QImage& image)                                                       = 0;
    virtual void pushTransformation(const QImage& image, const QString& description = QStringLiteral("")) = 0;
    virtual void setOptionsWidget(QWidget* widget)                                                        = 0;
    virtual void setToolOptionsWidget(QWidget* widget)                                                    = 0;
    virtual void reset()                                                                                  = 0;
    virtual bool selectionContains(const QPointF& p)                                                      = 0;
};

class EditorPlugin : public QObject {
    Q_OBJECT
  public:
    EditorPlugin(Editor* editor);

  protected:
    Editor* editor;
};

#endif
