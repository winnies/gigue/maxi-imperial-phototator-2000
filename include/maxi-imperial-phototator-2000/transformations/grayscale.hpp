#ifndef MAXI_IMPERIAL_PHOTOTATOR_2000_TRANSFORMATIONS_GRAYSCALE_HPP
#define MAXI_IMPERIAL_PHOTOTATOR_2000_TRANSFORMATIONS_GRAYSCALE_HPP

#include <maxi-imperial-phototator-2000/editor.hpp>

class GrayscalePlugin : public EditorPlugin {
  public:
    GrayscalePlugin(Editor* editor);

  private:
    void triggered();
};

#endif
