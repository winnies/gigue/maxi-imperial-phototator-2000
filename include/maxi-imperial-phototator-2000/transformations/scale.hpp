#ifndef MAXI_IMPERIAL_PHOTOTATOR_2000_TRANSFORMATIONS_SCALE_HPP
#define MAXI_IMPERIAL_PHOTOTATOR_2000_TRANSFORMATIONS_SCALE_HPP

#include <QPointer>
#include <maxi-imperial-phototator-2000/editor.hpp>
#include <maxi-imperial-phototator-2000/transformations/optionswidget.hpp>

namespace Ui {
class ScaleWidget;
} // namespace Ui

class ScaleWidget : public OptionsWidget {
    Q_OBJECT

    struct Options {
        QSize size;
        Qt::AspectRatioMode aspectRatioMode;
        Qt::TransformationMode transformMode;

        Options(const QSize& size,
                Qt::AspectRatioMode aspectRatioMode  = Qt::IgnoreAspectRatio,
                Qt::TransformationMode transformMode = Qt::FastTransformation);
    };

  public:
    explicit ScaleWidget(Editor* editor, QWidget* parent = nullptr);
    ~ScaleWidget() override;

  private:
    QString name() const;
    void optionsToWidget();
    void widgetToOptions();
    QImage transform(const QImage& source) const;

  protected slots:
    void reset() override;
    void canceled() override;
    void applied() override;

  private:
    void optionsChanged();

  private:
    Ui::ScaleWidget* ui;
    Editor* editor;

    Options options;
};

class ScalePlugin : public EditorPlugin {
  public:
    ScalePlugin(Editor* editor);
    ~ScalePlugin() override;

  private:
    void triggered();

    QPointer<ScaleWidget> scaleWidget;
};

#endif
