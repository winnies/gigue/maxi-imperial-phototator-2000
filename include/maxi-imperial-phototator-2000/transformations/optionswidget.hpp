#ifndef MAXI_IMPERIAL_PHOTOTATOR_2000_TRANSFORMATIONS_OPTIONSWIDGET_HPP
#define MAXI_IMPERIAL_PHOTOTATOR_2000_TRANSFORMATIONS_OPTIONSWIDGET_HPP

#include <QWidget>

namespace Ui {
class OptionsWidget;
} // namespace Ui

class OptionsWidget : public QWidget {
    Q_OBJECT

  public:
    explicit OptionsWidget(const QString& title, QWidget* parent = nullptr);
    ~OptionsWidget() override;

  protected slots:
    virtual void reset();
    virtual void canceled();
    virtual void applied();

  protected:
    Ui::OptionsWidget* ui;
};

#endif
