#ifndef MAXI_IMPERIAL_PHOTOTATOR_2000_EDITION_BUTTONCONTAINER_HPP
#define MAXI_IMPERIAL_PHOTOTATOR_2000_EDITION_BUTTONCONTAINER_HPP

#include <QPointer>
#include <QWidget>

namespace Ui {
class ButtonContainer;
} // namespace Ui

class QPushButton;
class ChooseColor;

class ButtonContainer : public QWidget {
    Q_OBJECT

  public:
    explicit ButtonContainer(QWidget* parent = nullptr);
    void setOptionsWidget(QWidget* widget);

    ~ButtonContainer() override;

  protected:
    Ui::ButtonContainer* ui;

  private:
    QLayout* m_optionsWidget;
    ChooseColor* m_chooseColor;

  private slots:
    void swapStack();

  signals:
    void selectionPressed();
    void colorChanged(int);
};

#endif
