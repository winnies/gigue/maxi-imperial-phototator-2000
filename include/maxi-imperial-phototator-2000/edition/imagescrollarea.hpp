#ifndef MAXI_IMPERIAL_PHOTOTATOR_2000_EDITION_IMAGESCROLLAREA_HPP
#define MAXI_IMPERIAL_PHOTOTATOR_2000_EDITION_IMAGESCROLLAREA_HPP

#include <QScrollArea>

class ImageScrollArea : public QScrollArea {
    Q_OBJECT

  public:
    ImageScrollArea(QWidget* parent = nullptr);
    ~ImageScrollArea() override = default;

  protected:
    void wheelEvent(QWheelEvent* event) override;
};

#endif
