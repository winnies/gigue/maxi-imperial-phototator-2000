#ifndef MAXI_IMPERIAL_PHOTOTATOR_2000_EDITION_CHOOSECOLOR_HPP
#define MAXI_IMPERIAL_PHOTOTATOR_2000_EDITION_CHOOSECOLOR_HPP

#include <QWidget>

namespace Ui {
class ChooseColor;
} // namespace Ui

class ChooseColor : public QWidget {
    Q_OBJECT

  public:
    explicit ChooseColor(QWidget* parent = nullptr);
    // QColor getColor();
    ~ChooseColor() override;

  protected:
    Ui::ChooseColor* ui;

  signals:
    void colorChanged(int);
};

#endif
