#ifndef MAXI_IMPERIAL_PHOTOTATOR_2000_EDITION_SELECTRECT_HPP
#define MAXI_IMPERIAL_PHOTOTATOR_2000_EDITION_SELECTRECT_HPP

#include <QDebug>
#include <QMouseEvent>
#include <QPainter>
#include <QWidget>

class SelectRect : public QWidget {
    Q_OBJECT

  public:
    SelectRect(QWidget* parent = nullptr);
    ~SelectRect() override = default;

    bool contains(const QPointF& p);
    void changeColor(int i);
    void setTarget(QWidget* target);

  protected:
    void paintEvent(QPaintEvent* event) override;
    void mousePressEvent(QMouseEvent* event) override;
    void mouseReleaseEvent(QMouseEvent* event) override;
    void mouseMoveEvent(QMouseEvent* event) override;

  private:
    QPoint mapToGlobal(const QPointF& p) const;
    QPointF mapFromGlobal(const QPoint& p) const;

    QRect mapToGlobal(const QRectF& p) const;
    QRectF mapFromGlobal(const QRect& p) const;

    //    void setSelection(QRect absoluteSelection);

  private:
    QRectF relativeSelection;

    // QRect absoluteSelection;
    QPen pen;

    enum class SelectState {
        None,
        BottomLeft,
        BottomRight,
        TopLeft,
        TopRight,
        Center,
        middleTop,
        middleBottom,
        middleLeft,
        middleRight
    };

    SelectState selectState{SelectState::None};
    int taillepoignee{10};
    // QPoint middleTop, middleBottom, middleLeft, middleRight;

    QWidget* target;
};

#endif
