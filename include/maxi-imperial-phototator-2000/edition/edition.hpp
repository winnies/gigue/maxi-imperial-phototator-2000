#ifndef MAXI_IMPERIAL_PHOTOTATOR_2000_EDITION_EDITION_HPP
#define MAXI_IMPERIAL_PHOTOTATOR_2000_EDITION_EDITION_HPP

#include <QPointer>
#include <QWidget>

#include <maxi-imperial-phototator-2000/editor.hpp>

class QStatusBar;
class QStackedWidget;
class WidgetImage;
class QVBoxLayout;
class QHBoxLayout;
class SelectRect;
class ImageContainer;

class Edition : public Editor {
    Q_OBJECT

  public:
    explicit Edition(QWidget* parent = nullptr);
    explicit Edition(const QVector<QString>& imagesPath, QWidget* parent = nullptr);
    ~Edition() override;

    QMenuBar* menuBar() override;
    QToolBar* toolBar() override;
    QMenuBar* getMenuBar();
    QStatusBar* getStatusBar();
    const QImage& currentImage() const override;
    void setImageShown(const QImage& image) override;
    void pushTransformation(const QImage& image, const QString& description = QStringLiteral("")) override;
    void setOptionsWidget(QWidget* widget) override;
    void setToolOptionsWidget(QWidget* widget) override;
    bool selectionContains(const QPointF& p) override;
    void reset() override;

    bool saveImages();

  signals:
    void swapBack();
    void saveTransformation(const QVector<QString>& pathImages);

  private:
    void setCurrentImage(const QImage& image);
    void setCurrentPathImage(const QString& imagePath);
    void returnWorskpace();

  private:
    QMenuBar* m_menuBar;
    QVBoxLayout* m_layout;
    QToolBar* m_toolBar;
    WidgetImage* m_widgetImage;
    QStackedWidget* m_stackedWidget;
    QLayout* m_optionsWidget;
    QHash<QString, QImage> m_images;

    ImageContainer* imgContainer;

    bool imageChange;

    struct Transformation {
        const QImage image;
        const QString description;

        Transformation(const QImage& image, const QString& description) : image(image), description(description) {}
    };

    QImage m_currentImage;
    QString m_currentPathImage;
    std::vector<Transformation> transformations;
    QPointer<QWidget> options;
};

#endif
