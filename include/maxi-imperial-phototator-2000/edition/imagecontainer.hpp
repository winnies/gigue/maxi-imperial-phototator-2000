#ifndef MAXI_IMPERIAL_PHOTOTATOR_2000_EDITION_IMAGECONTAINER_HPP
#define MAXI_IMPERIAL_PHOTOTATOR_2000_EDITION_IMAGECONTAINER_HPP

#include <QListWidget>
#include <QListWidgetItem>
#include <QObject>
#include <QVBoxLayout>
#include <QWidget>

class ImageContainer : public QWidget {
    Q_OBJECT
  public:
    QVBoxLayout* mainBox;
    QListWidget* container;
    QListWidgetItem* imageSelect;
    QHash<QString, QListWidgetItem*> arrayPicture;

  public:
    explicit ImageContainer(QWidget* parent = nullptr);
    void addPictureWorksapce(QString& path);
    void selectImage(QListWidgetItem* item);

  signals:
    void imageSelected(QString pathImage);

  public slots:
    void resizeEvent(QResizeEvent* event) override;
};

#endif
