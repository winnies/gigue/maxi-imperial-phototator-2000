#ifndef MAXI_IMPERIAL_PHOTOTATOR_2000_EDITION_WIDGETIMAGE_HPP
#define MAXI_IMPERIAL_PHOTOTATOR_2000_EDITION_WIDGETIMAGE_HPP

#include <QDebug>
#include <QLabel>
#include <QScrollArea>
#include <QScrollBar>
#include <QShortcut>
#include <QVBoxLayout>
#include <QWidget>

#include <maxi-imperial-phototator-2000/edition/imagescrollarea.hpp>
#include <maxi-imperial-phototator-2000/edition/selectrect.hpp>

class WidgetImage : public QWidget {
    Q_OBJECT

  public:
    WidgetImage(QWidget* parent = nullptr);
    ~WidgetImage() override = default;

    bool selectionContains(const QPointF& p);

  public slots:
    void setImage(const QImage& image);
    void changeSelectionColor(int i);
    void setSelection();

  protected:
    void wheelEvent(QWheelEvent* event) override;
    void resizeEvent(QResizeEvent* event) override;

  private slots:
    void zoomIn();
    void zoomOut();

  private:
    void scaleImage(double factor);

    QLabel* m_imageLabel;
    ImageScrollArea* m_scrollArea;
    QVBoxLayout* m_layout;
    SelectRect* m_selection;
    double scaleFactor = 1;
};
#endif
