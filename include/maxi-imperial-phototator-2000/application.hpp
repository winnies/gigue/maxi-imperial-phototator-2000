#ifndef MAXI_IMPERIAL_PHOTOTATOR_2000_APPLICATION_HPP
#define MAXI_IMPERIAL_PHOTOTATOR_2000_APPLICATION_HPP

#include <QMainWindow>

class QStackedWidget;
class WorkspaceWidget;
class Edition;
class QCloseEvent;

class Application : public QMainWindow {
  public:
    explicit Application(QWidget* parent = nullptr);

  private:
    QStackedWidget* stackWidget;

    WorkspaceWidget* workspaceWidget;
    Edition* editionWidget;

    void toWorkspaceWidget();
    void toEditionWidget(const QVector<QString>& imagesPath);

  protected:
    void closeEvent(QCloseEvent* event) override;
};

#endif
