#!/bin/bash

export PATH=/opt/Qt/5.13.0/gcc_64/bin:$PATH

source scripts/install_cmake.sh
source scripts/install_ecm.sh

cmake -S . -B build
cmake --build build --parallel $(nproc)

echo 'executable at build/maxi-imperial-phototator-2000'
