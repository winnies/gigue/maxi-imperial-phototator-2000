#!/bin/bash

if [ -z ${DEPS+x} ]; then
    export DEPS=$(pwd)/deps
    echo "DEPS = ${DEPS}"
fi

mkdir -p ${DEPS}
