QT       += core gui widgets

TARGET = maxi-imperial-phototator-2000
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++14 exceptions_off no_include_pwd stl warn_on

SOURCES += \
        src/application.cpp \
        src/edition/buttoncontainer.cpp \
        src/edition/choosecolor.cpp \
        src/edition/edition.cpp \
        src/edition/imagecontainer.cpp \
        src/edition/imagescrollarea.cpp \
        src/edition/selectrect.cpp \
        src/edition/widgetimage.cpp \
        src/editor.cpp \
        src/main.cpp \
        src/transformations/grayscale.cpp \
        src/transformations/optionswidget.cpp \
        src/transformations/scale.cpp \
        src/workspace/workspace.cpp \
        src/workspace/workspacewidget.cpp

HEADERS +=  \
    include/maxi-imperial-phototator-2000/application.hpp \
    include/maxi-imperial-phototator-2000/edition/buttoncontainer.hpp \
    include/maxi-imperial-phototator-2000/edition/choosecolor.hpp \
    include/maxi-imperial-phototator-2000/edition/edition.hpp \
    include/maxi-imperial-phototator-2000/edition/imagecontainer.hpp \
    include/maxi-imperial-phototator-2000/edition/imagescrollarea.hpp \
    include/maxi-imperial-phototator-2000/edition/selectrect.hpp \
    include/maxi-imperial-phototator-2000/edition/widgetimage.hpp \
    include/maxi-imperial-phototator-2000/editor.hpp \
    include/maxi-imperial-phototator-2000/transformations/grayscale.hpp \
    include/maxi-imperial-phototator-2000/transformations/optionswidget.hpp \
    include/maxi-imperial-phototator-2000/transformations/scale.hpp \
    include/maxi-imperial-phototator-2000/workspace/workspace.hpp \
    include/maxi-imperial-phototator-2000/workspace/workspacewidget.hpp

FORMS +=  \
    src/edition/buttoncontainer.ui \
    src/edition/choosecolor.ui \
    src/transformations/optionswidget.ui \
    src/transformations/scalewidget.ui

INCLUDEPATH += $$PWD/include

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

