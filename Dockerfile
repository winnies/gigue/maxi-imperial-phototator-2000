FROM ubuntu:xenial

SHELL ["/bin/bash", "-c"]
COPY scripts /scripts
RUN set -ex; \
    apt update && apt -y install software-properties-common wget; \
    add-apt-repository -y ppa:ubuntu-toolchain-r/test; \
    add-apt-repository -y ppa:beineri/opt-qt-5.12.3-xenial; \
    wget -O - https://apt.llvm.org/llvm-snapshot.gpg.key | apt-key add -; \
    apt-add-repository -y "deb http://apt.llvm.org/xenial/ llvm-toolchain-xenial-9 main"; \
    apt update && apt -y install gcc-9 g++-9 make git qt512-meta-minimal qt512base qt512declarative qt512tools zlib1g-dev extra-cmake-modules libgl1-mesa-dev bzip2 xz-utils doxygen graphviz clang-format-9 clang-tidy-9; \
    update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-9 60 --slave /usr/bin/g++ g++ /usr/bin/g++-9; \
    update-alternatives --config gcc; \
    chmod u+x /scripts/update-alternatives-clang.sh; \
    /scripts/update-alternatives-clang.sh 9 60; \
    source /scripts/setup_ci.sh
